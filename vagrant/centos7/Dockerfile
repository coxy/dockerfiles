FROM centos:centos7
MAINTAINER Chris Cox <chris@bashton.com>

RUN yum clean all
RUN yum swap fakesystemd systemd
RUN yum -y update
RUN yum -y install https://anorien.csc.warwick.ac.uk/mirrors/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
RUN yum -y install https://bashton-yumrepo.s3.amazonaws.com/el7/yum-plugin-s3-2.0-1.noarch.rpm
RUN yum -y install https://bashton-yumrepo.s3.amazonaws.com/el7/daemonize-1.7.5-1.el7.centos.x86_64.rpm
RUN yum -y install https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
RUN yum -y install plvm2 xfsprogs tcpdump curl wget strace ltrace man telnet procps nc parted redhat-lsb kernel-devel initscript python-pip puppet-agent sudo openssh-server openssh-clients
RUN pip install awscli boto

# Add a way to run Puppet via MCO
COPY files/puppet-masterless-mco /usr/bin/puppet-masterless-mco
RUN chmod 744 /usr/bin/puppet-masterless-mco
RUN mkdir /usr/libexec/mcollective

# Add vagrant user and key
RUN useradd --create-home -s /bin/bash vagrant
RUN echo -n 'vagrant:vagrant' | chpasswd
RUN echo 'vagrant ALL = NOPASSWD: ALL' > /etc/sudoers.d/vagrant
RUN chmod 440 /etc/sudoers.d/vagrant
RUN mkdir -p /home/vagrant/.ssh
RUN chmod 700 /home/vagrant/.ssh
RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key" > /home/vagrant/.ssh/authorized_keys
RUN chmod 600 /home/vagrant/.ssh/authorized_keys
RUN chown -R vagrant:vagrant /home/vagrant/.ssh
RUN sed -i -e 's/Defaults.*requiretty/#&/' /etc/sudoers
RUN sed -i -e 's/\(UsePAM \)yes/\1 no/' /etc/ssh/sshd_config
RUN systemctl enable sshd.service
CMD ["/usr/sbin/init"]
